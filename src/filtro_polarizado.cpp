#include <iostream>

#include "imagem.hpp"
#include "filtro_polarizado.hpp"

using namespace std;

void Filtro_polarizado::aplicar(Imagem * imagem_1) {
  string nome;
  unsigned int max_color = imagem_1->getMax_cor();
  pixel **conteudo = new pixel * [imagem_1->getDim_linhas()];
  for(int i = 0; i < imagem_1->getDim_colunas(); ++i) {
    conteudo[i] = new pixel [imagem_1->getDim_linhas()];
    for(int j = 0; j < imagem_1->getDim_linhas(); ++j){
      if(imagem_1->getConteudo_red(i,j) < max_color/2){
          conteudo[i][j].red = 0;
      }else{
          conteudo[i][j].red = max_color;
      }

      if(imagem_1->getConteudo_green(i,j) < max_color/2){
          conteudo[i][j].green = 0;
      }else{
          conteudo[i][j].green = max_color;
      }

      if(imagem_1->getConteudo_blue(i,j) < max_color/2){
          conteudo[i][j].blue = 0;
      }else{
          conteudo[i][j].blue = max_color;
      }
    }
  }
  cout << "Entre com o nome da nova imagem: " << endl;
  cin >> nome;
  imagem_1->gravarImagem(conteudo, nome);
}
