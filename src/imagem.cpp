#include "imagem.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

Imagem::Imagem(string nome) {
    this->nome = nome;
    open_file(nome);
}

void Imagem::setComentario(string comentario) {
  this->comentario = comentario;
}

void Imagem::setTipo(string tipo) {
  if(tipo.compare("P6") != 0) {
    cerr << "Formato de imagem não suportado!" << endl;
  }
  this->tipo = tipo;
}

void Imagem::setDim_linhas(int dim_linhas) {
  this->dim_linhas = dim_linhas;
}

void Imagem::setDim_colunas(int dim_colunas) {
  this->dim_colunas = dim_colunas;
}

void Imagem::setMax_cor(int max_cor) {
  this->max_cor = max_cor;
}

void Imagem::setConteudo(pixel ** conteudo) {
  this->conteudo = conteudo;
}

unsigned char Imagem::getConteudo_red(int i, int j) {
  return conteudo[i][j].red;
}

unsigned char Imagem::getConteudo_green(int i, int j) {
  return conteudo[i][j].green;
}

unsigned char Imagem::getConteudo_blue(int i, int j) {
  return conteudo[i][j].blue;
}

pixel ** Imagem::getConteudo(){
  return conteudo;
}

void Imagem::Comentario() {
  string comentario = getComentario();
  if (comentario[0] == '#') {
    cout << "Comentario: " << comentario << endl;
  }
}

void Imagem::open_file(string nome){
  string line = "";
  char c;
  int i, j, number = 0;
  nome = "files/"+nome+".ppm";
  ifstream infile(nome.c_str());
  if (infile.is_open() == 0) {
   cout << "Erro, Não foi possivel abrir o arquivo!" << endl;
  }
  infile >> line;
  setTipo(line);
  infile >> line;
  if( line[0] == '#') {
    setComentario(line);
    infile >> number;
  }
  else {
    stringstream buffer(line);
    buffer >> number;
  }
  setDim_linhas(number);
  infile >> line;
  if( line[0] == '#') {
    setComentario(line);
    infile >> number;
  }
  else {
    stringstream buffer(line);
    buffer >> number;
  }
  setDim_colunas(number);
  infile >> number;
  setMax_cor(number);
  getline(infile,line);

  pixel **conteudo = new pixel * [getDim_linhas()];
  for(i = 0; i < getDim_colunas(); i++) {
  conteudo[i] = new pixel [getDim_linhas()];
    for (j = 0; j < getDim_linhas(); j++) {
      infile.get(c);
      conteudo[i][j].red = c;
      infile.get(c);
      conteudo[i][j].green = c;
      infile.get(c);
      conteudo[i][j].blue = c;
    }
  }
  setConteudo(conteudo);
  infile.close();
}

void Imagem::gravarImagem(pixel ** conteudo, string nomeFiltro) {
  nomeFiltro = "files/"+nomeFiltro+".ppm";
  ofstream imagemFiltro(nomeFiltro.c_str());

  imagemFiltro << getTipo() << endl;
  imagemFiltro << getDim_linhas() << endl;
  imagemFiltro << getDim_colunas() << endl;
  imagemFiltro << getMax_cor() << endl;
  for(int linha = 0;linha < getDim_colunas(); linha++) {
    for(int coluna = 0;coluna < getDim_linhas(); coluna++) {
      imagemFiltro << conteudo[linha][coluna].red;
      imagemFiltro << conteudo[linha][coluna].green;
      imagemFiltro << conteudo[linha][coluna].blue;
    }
  }
imagemFiltro.close();
}
