#include <iostream>

#include "imagem.hpp"
#include "filtro_greyscale.hpp"

using namespace std;

void Filtro_greyscale::aplicar(Imagem * imagem_1) {
  string nome;
  unsigned int grayscale_value;
  pixel ** conteudo = new pixel * [imagem_1->getDim_linhas()];
  for(int i = 0; i < imagem_1->getDim_colunas(); ++i) {
    conteudo[i] = new pixel [imagem_1->getDim_linhas()];
    for(int j = 0; j < imagem_1->getDim_linhas(); ++j){
      grayscale_value = (0.299 * imagem_1->getConteudo_red(i, j)) + (0.587 * imagem_1->getConteudo_green(i, j)) + (0.144 * imagem_1->getConteudo_blue(i, j));

      conteudo[i][j].red = grayscale_value;
      conteudo[i][j].green = grayscale_value;
      conteudo[i][j].blue = grayscale_value;
    }
  }
  cout << "Entre com o nome da nova imagem: " << endl;
  cin >> nome;
  imagem_1->gravarImagem(conteudo, nome);
}
