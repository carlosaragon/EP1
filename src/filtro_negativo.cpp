#include <iostream>

#include "imagem.hpp"
#include "filtro_negativo.hpp"

using namespace std;

void Filtro_negativo::aplicar(Imagem * imagem_1) {
  string nome;
  pixel **conteudo = new pixel * [imagem_1->getDim_linhas()];
  for(int i = 0; i < imagem_1->getDim_colunas(); ++i) {
    conteudo[i] = new pixel [imagem_1->getDim_linhas()];
    for(int j = 0; j < imagem_1->getDim_linhas(); ++j){
        conteudo[i][j].red = (imagem_1->getMax_cor() - imagem_1->getConteudo_red(i, j));
        conteudo[i][j].green = (imagem_1->getMax_cor() - imagem_1->getConteudo_green(i, j));
        conteudo[i][j].blue = (imagem_1->getMax_cor() - imagem_1->getConteudo_blue(i, j));
    }
  }
  cout << "Entre com o nome da nova imagem: " << endl;
  cin >> nome;
  imagem_1->gravarImagem(conteudo, nome);
}
