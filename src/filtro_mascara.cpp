#include <iostream>

#include "imagem.hpp"
#include "filtro_mascara.hpp"

using namespace std;

void Filtro_mascara::aplicar(Imagem * imagem_1, int limite) {
  string nome;
  unsigned int divisor;
  divisor = limite * limite;
  unsigned int valuer, valueg, valueb;
  pixel ** conteudo = new pixel * [imagem_1->getDim_linhas()];
  for(int i = 0; i < imagem_1->getDim_colunas(); ++i)
    conteudo[i] = new pixel [imagem_1->getDim_linhas()];
  for(int linha = 0;linha < imagem_1->getDim_colunas();linha++) {
    for(int coluna = 0;coluna < imagem_1->getDim_linhas();coluna++){
      conteudo[linha][coluna].red = imagem_1->getConteudo_red(linha, coluna);
      conteudo[linha][coluna].green = imagem_1->getConteudo_green(linha, coluna);
      conteudo[linha][coluna].blue = imagem_1->getConteudo_blue(linha, coluna);
    }
  }

  limite = limite/2;
  for(int linha = limite; linha < (imagem_1->getDim_colunas() - limite); ++linha){
      for(int coluna = limite; coluna  < (imagem_1->getDim_linhas() - limite); ++coluna) {
             valuer = 0;
             valueg = 0;
             valueb = 0;
             for(int x = linha - limite; x <= linha + limite; x++) {
                for(int y = coluna - limite; y <= coluna + limite; y++) {
                      valuer += conteudo[x][y].red;
                      valueg += conteudo[x][y].green;
                      valueb += conteudo[x][y].blue;
                }
             }
             valuer /= divisor;
             conteudo[linha][coluna].red = valuer;
             valueg /= divisor;
             conteudo[linha][coluna].green = valueg;
             valueb /= divisor;
             conteudo[linha][coluna].blue = valueb;
      }
  }
  cout << "Entre com o nome da nova imagem: " << endl;
  cin >> nome;
  imagem_1->gravarImagem(conteudo, nome);
}
