#include <iostream>
// #include </home/carlos/Documents/Orientação a Objetos/EP1/linux-conio/conio.h>

#include "imagem.hpp"
#include "filtro.hpp"
#include "filtro_negativo.hpp"
#include "filtro_greyscale.hpp"
#include "filtro_polarizado.hpp"
#include "filtro_mascara.hpp"

int main() {
  string nome;
  char option = 1;
  while(option != 0) {
    cout << "------------------------------------" << endl;
    cout << "|1 - Aplicar filtro Negativo.      |\n|2 - Aplicar filtro Polarizado.    |\n|3 - Aplicar filtro Branco e Preto.|\n|4 - Aplicar filtro de Média.      |\n|0 - Sair                          |\n";
    cout << "------------------------------------" << endl;
    cin >> option;
    // system("clear");

    switch (option) {
      case '1': {
        cout << "Entre com o nome do arquivo: " << endl;
        cin >> nome;
        Imagem *imagem_1 = new Imagem(nome);
        cout << "Nome do arquivo: " << imagem_1->getNome() << endl;
        cout << "Formato do arquivo: " << imagem_1->getTipo() << endl;
        imagem_1->Comentario();
        cout << "Dimensões do arquivo: " << imagem_1->getDim_linhas() << "x" << imagem_1->getDim_colunas() << endl;
        cout << "Intervalo de cor do arquivo: " << imagem_1->getMax_cor() << endl;

        Filtro_negativo fn;
        fn.aplicar(imagem_1);
        break;
      }

      case '2': {
        cout << "Entre com o nome do arquivo: " << endl;
        cin >> nome;
        Imagem *imagem_1 = new Imagem(nome);
        cout << "Nome do arquivo: " << imagem_1->getNome() << endl;
        cout << "Formato do arquivo: " << imagem_1->getTipo() << endl;
        imagem_1->Comentario();
        cout << "Dimensões do arquivo: " << imagem_1->getDim_linhas() << "x" << imagem_1->getDim_colunas() << endl;
        cout << "Intervalo de cor do arquivo: " << imagem_1->getMax_cor() << endl;

        Filtro_polarizado fp;
        fp.aplicar(imagem_1);
        break;
      }

      case '3': {
        cout << "Entre com o nome do arquivo: " << endl;
        cin >> nome;
        Imagem *imagem_1 = new Imagem(nome);
        cout << "Nome do arquivo: " << imagem_1->getNome() << endl;
        cout << "Formato do arquivo: " << imagem_1->getTipo() << endl;
        imagem_1->Comentario();
        cout << "Dimensões do arquivo: " << imagem_1->getDim_linhas() << "x" << imagem_1->getDim_colunas() << endl;
        cout << "Intervalo de cor do arquivo: " << imagem_1->getMax_cor() << endl;

        Filtro_greyscale fg;
        fg.aplicar(imagem_1);
        break;
      }

      case '4': {
        cout << "Entre com o nome do arquivo: " << endl;
        cin >> nome;
        Imagem *imagem_1 = new Imagem(nome);
        cout << "Nome do arquivo: " << imagem_1->getNome() << endl;
        cout << "Formato do arquivo: " << imagem_1->getTipo() << endl;
        imagem_1->Comentario();
        cout << "Dimensões do arquivo: " << imagem_1->getDim_linhas() << "x" << imagem_1->getDim_colunas() << endl;
        cout << "Intervalo de cor do arquivo: " << imagem_1->getMax_cor() << endl;
        int tamanho;
        cout << "Escolha a opção do filtro: \n3 - 3x3:\n5 - 5x5:\n7 - 7x7:" << endl;
        cin >> tamanho;
        Filtro_mascara fm;
        fm.aplicar(imagem_1, tamanho);
        break;

      }

      case '0': {
        option = 0;
        break;
      }
    }
  }


  return 0;
}
