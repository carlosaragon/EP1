#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>

using namespace std;

typedef struct pixel_{
  unsigned char red;
  unsigned char green;
  unsigned char blue;
}pixel;

class Imagem {
private:
  string nome ;
  string tipo;
  string comentario;
  int dim_linhas;
  int dim_colunas;
  unsigned int max_cor;
  pixel ** conteudo;

public:
  Imagem(string nome);
  ~Imagem();
  void setTipo(string tipo);
  void setComentario(string comentario);
  void setDim_linhas(int dim_linhas);
  void setDim_colunas(int dim_colunas);
  void setMax_cor(int max_cor);
  void setConteudo(pixel ** conteudo);

  string getNome() {return nome;};
  string getTipo() {return tipo;};
  string getComentario() {return comentario;};
  int getDim_linhas() {return dim_linhas;};
  int getDim_colunas() {return dim_colunas;};
  unsigned int getMax_cor() {return max_cor;};
  unsigned char getConteudo_red(int i, int j);
  unsigned char getConteudo_green(int i, int j);
  unsigned char getConteudo_blue(int i, int j);
  pixel ** getConteudo();

  void open_file(string nome);
  void gravarImagem(pixel ** conteudo, string nome);
  void Comentario();
};

#endif
