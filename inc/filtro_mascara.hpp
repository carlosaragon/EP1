#ifndef FILTROMASCARA_HPP
#define FILTROMASCARA_HPP

#include "filtro.hpp"

class Filtro_mascara : public Filtro {
public:
  Filtro_mascara() {};
  void aplicar(Imagem * imagem_1, int limite);
};
#endif
