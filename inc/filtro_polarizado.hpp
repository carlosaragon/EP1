#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include "filtro.hpp"

class Filtro_polarizado : Filtro {
public:
  Filtro_polarizado() {};
  void aplicar(Imagem * imagem_1);
};
#endif
