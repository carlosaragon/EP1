#ifndef FITROGREYSCALE_HPP
#define FILTROGREYSCALE_HPP

#include "filtro.hpp"

class Filtro_greyscale : public Filtro {
public:
  Filtro_greyscale() {};
  void aplicar(Imagem * imagem);
};
#endif
