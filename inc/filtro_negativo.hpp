#ifndef FITRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include "filtro.hpp"

class Filtro_negativo : public Filtro {
public:
  Filtro_negativo() {};
  void aplicar(Imagem * imagem);
};
#endif
