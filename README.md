# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

# Onde inserir as imagens

As imagens a serem filtradas devem estar na pasta 'files' que se encontra na raiz do programa, as imagens geradas após a aplicação de filtros também serão geradas nessa pasta.

# Compilação e Execução

As etapas para a compilação e execução desse programa são:
Abrir o terminal na pasta EP1
Executar o comando 'make clean', para assegurar uma compilação limpa e correta.
Executar o comando 'make', para compilar.
Executar o comando 'make run', para executar o programa.
Escolher a opção de filtro desejada.
Entrar com o nome da imagem a qual será aplicada o filtro.
Escolher o tamanho do filtro no caso do filtro de média.
Entrar com o nome que devera ter o arquivo de saída gerado após a aplicação do filtro.

 
